//
//  SwipeTabVC.m
//  TestTab
//
//  Created by hankers on 15/8/24.
//  Copyright (c) 2015年 hankers. All rights reserved.
//

#import "SwipeTabVC.h"

@interface SwipeTabVC ()

@end

@implementation SwipeTabVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tappedRightButton:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tappedLeftButton:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tappedRightButton:(UIGestureRecognizer*)gestureRecognizer
{
    // Get views. controllerIndex is passed in as the controller we want to go to.
    [self moveTabviewRight:YES];
}

- (void)tappedLeftButton:(UIGestureRecognizer*)gestureRecognizer
{
    [self moveTabviewRight:NO];
}

-(void) moveTabviewRight:(BOOL)right {
    
    NSInteger controllerIndex = self.selectedIndex + (right ? 1 : -1);
    
    if(controllerIndex < 0 || controllerIndex >= self.viewControllers.count)
        return;
    
    // Get the views.
    UIView * fromView = self.selectedViewController.view;
    UIView * toView = [[self.viewControllers objectAtIndex:controllerIndex] view];
    
    // Get the size of the view area.
    CGRect viewSize = fromView.frame;
    BOOL scrollRight = right;
    
    // Add the to view to the tab bar view.
    [fromView.superview addSubview:toView];
    
    const NSInteger W = self.view.frame.size.width;
    
    // Position it off screen.
    toView.frame = CGRectMake((scrollRight ? W : -W), viewSize.origin.y, W, viewSize.size.height);
    
    [UIView animateWithDuration:0.3
                     animations: ^{
                         
                         // Animate the views on and off the screen. This will appear to slide.
                         fromView.frame =CGRectMake((scrollRight ? -W : W), viewSize.origin.y, W, viewSize.size.height);
                         toView.frame =CGRectMake(0, viewSize.origin.y, W, viewSize.size.height);
                     }
     
                     completion:^(BOOL finished) {
                         if (finished) {
                             
                             // Remove the old view from the tabbar view.
                             [fromView removeFromSuperview];
                             self.selectedIndex = controllerIndex;
                         }
                     }];
}

//-(void)builtinAction {
//    
//    UIView * fromView = self.selectedViewController.view;
//    UIView * toView = [[self.viewControllers objectAtIndex:self.selectedIndex + 1] view];
//
//    // Transition using a page curl.
//    [UIView transitionFromView:fromView
//                        toView:toView
//                      duration:0.5
//                       options:(self.selectedIndex + 1 > self.selectedIndex ? UIViewAnimationOptionTransitionFlipFromRight : UIViewAnimationOptionTransitionFlipFromLeft)
//                    completion:^(BOOL finished) {
//                        if (finished) {
//                            self.selectedIndex = self.selectedIndex + 1;
//                        }
//                    }];
//}

//-(void)tabBarController:(UITabBarController *)tabBarControllerThis didSelectViewController:(UIViewController *)viewController
//{
//    [UIView transitionWithView:viewController.view
//                      duration:0.1
//                       options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^(void){
//                    } completion:^(BOOL finished){
//                        [UIView beginAnimations:@"animation" context:nil];
//                        [UIView setAnimationDuration:0.7];
//                        [UIView setAnimationBeginsFromCurrentState:YES];
//                        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
//                                               forView:viewController.view
//                                                 cache:NO];
//                        [UIView commitAnimations];
//                    }];
//}

@end
